using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField] private Animator doorAnimator;
    [SerializeField] public bool isOpen = false;
    public bool keyRequired = false;
    public int keyCode = 0;

    private void Start()
    {
        doorAnimator = GetComponent<Animator>();
    }

    public void Toggle()
    {
        if (isOpen == false)
        {
            isOpen = true;
            doorAnimator.SetTrigger("Open");
        }
        else
        {
            isOpen = false;
            doorAnimator.SetTrigger("Close");
        }
    }
}
