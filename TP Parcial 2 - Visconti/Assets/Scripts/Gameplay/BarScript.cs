using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarScript : MonoBehaviour
{
    public Slider slider;

    public void SetMaxFill(float amount)
    {
        slider.maxValue = amount;
        slider.value = amount;
    }
    public void SetFill(float amount)
    {
        slider.value = amount;
    }
}
