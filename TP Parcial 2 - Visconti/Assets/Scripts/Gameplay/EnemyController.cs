using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    [Header("Technical Properties")]
    Rigidbody rb;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] private float height;
    [SerializeField] private GameObject hitParticles;
    [SerializeField] private float health = 10;
    [Header("Awareness")]
    [SerializeField] private float sightDistance = 16f;
    [SerializeField] private float aggroDistance = 16f;
    [SerializeField] private LayerMask playerLayer;
    public bool isAware = false;
    public bool isAggroed = false;
    [SerializeField] private Material regularMat;
    [SerializeField] private Material aggroMat;
    [SerializeField] private float rotSpeed = 4f;
    private Transform player;
    [SerializeField] float strenght = 10f;
    [SerializeField] float attackCooldown = 0.5f;
    [SerializeField] float currCooldown = 0f;
    bool isAttacking = false;
    [Header("Movement")]
    //Move Variables
    [SerializeField] private float movSpeed;
    [SerializeField] private NavMeshAgent navMeshAgent;
    public bool staggered = false;
    public float staggerCooldown = 0.25f;
    [Header("Cheats")]
    public bool enabledAI = true;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        player = FindObjectOfType<PlayerController>().transform;
        navMeshAgent = GetComponent<NavMeshAgent>();
    }
    private void Update()
    {
        if (player.GetComponent<PlayerController>().inGame == false)
        {
            return;
        }
        if (player.GetComponent<PlayerController>().isTargetable == false)
        {
            Disengage();
        }
        else
        {
            //Check Awareness and look at player
            isAware = CheckPlayerNear(sightDistance);
            if (isAware) { Awareness(); }
            //Check Aggroness and engage/disengage against player
            isAggroed = CheckPlayerNear(aggroDistance);
            if (isAggroed && staggered == false) { Engage(); } else Disengage();
        }

        //Check Health and Die
        if (health <= 0) { Die(); }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("Touched Player");
            isAttacking = true;
            StartCoroutine(Attack());
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            isAttacking = false;
        }
    }

    bool Grounded()
    {
        return Physics.Raycast(transform.position, Vector3.down, height * 0.5f + 0.2f, groundLayer);
    }

    bool CheckPlayerNear(float distance)
    {
        bool lineOfSight = false;
        Ray ray = new Ray(rb.transform.position, player.position - rb.position);
        RaycastHit hit;
        Physics.Raycast(ray, out hit, distance);
        if (hit.transform == player)
        {
            lineOfSight = true;
        }
        Debug.DrawLine(rb.transform.position, rb.transform.position + rb.transform.forward * 3f, Color.red, 0.016f);
        return Physics.CheckSphere(transform.position, distance, playerLayer) && lineOfSight;
    }
    void Awareness()
    {
        StartCoroutine(LookAt());
    }
    private IEnumerator LookAt()
    {
        Quaternion lookRotation = Quaternion.LookRotation(player.position - transform.position);
        lookRotation.x = transform.rotation.x;
        lookRotation.z = transform.rotation.z;
        float time = 0;
        while (time < 1)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, time * Time.deltaTime);
            time += Time.deltaTime * rotSpeed;

            yield return null;
        }
    }
    private void Engage()
    {
        GetComponent<MeshRenderer>().material = aggroMat;
        if (Grounded() == true)
        {
            navMeshAgent.SetDestination(player.position);
        }
    }
    IEnumerator Attack()
    {
        PlayerController playerScript = player.GetComponent<PlayerController>();
        while (isAttacking == true)
        {
            if (currCooldown <= 0)
            {
                playerScript.InvokeTakeDamage(strenght);
                Debug.Log("Starting Enemy Cooldown");
                currCooldown = attackCooldown;
            }
            else
            {
                Debug.Log(currCooldown);
                currCooldown -= attackCooldown/2;
            }
            StartCoroutine(Stagger());
            yield return new WaitForSeconds(1f);
        }
    }
    void Disengage()
    {
        GetComponent<MeshRenderer>().material = regularMat;
        navMeshAgent.SetDestination(transform.position);
    }
    public void TakeDamage(float takenDamage)
    {
        //Random Sound between 3 choices
        string[] sound = { "EnemyPain0", "EnemyPain1", "EnemyPain2" };
        int i = UnityEngine.Random.Range(0, 3);
        AudioManager.instance.PlaySound(sound[i]);
        Instantiate(hitParticles, transform.position, Quaternion.identity);
        Instantiate(hitParticles, new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z), Quaternion.identity);
        Instantiate(hitParticles, new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z), Quaternion.identity);
        health -= takenDamage;
        StartCoroutine(Stagger());
    }
    IEnumerator Stagger()
    {
        staggered = true;
        yield return new WaitForSeconds(staggerCooldown);
        staggered = false;
    }
    void Die()
    {
        for (int i = 0; i < 10; i++)
        {
            Instantiate(hitParticles, transform.position, Quaternion.identity);
        }
        Destroy(rb.gameObject);
    }
}