using System;
using System.Collections;
using UnityEngine;
using TMPro;

public class CheatsManager : MonoBehaviour
{
    [SerializeField] Cheat[] cheatSheet;
    int currCharIndex = 0;
    string currChar;
    string currCheat = "";
    int expectedCheatId = 0;
    public static CheatsManager instance;
    [SerializeField] GameObject CheatBox;
    [SerializeField] PlayerController playerController;
    private float currHealthPH;
    private float maxHealthPH;
    private float exhaustRatePH;
    private float jumpExhaustionPH;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

    }
    private void Start()
    {
        CheatBox = GameObject.Find("CheatsLegend");
        playerController = GameObject.Find("Player").GetComponent<PlayerController>();
    }

    private void Update()
    {
        CheckInput();
    }

    void CheckInput()
    {
        if (Input.anyKeyDown)
        {
            if (Input.inputString.ToString().Length == 1)
            {
                currChar = Input.inputString.ToString();
                if (currCharIndex == 0)
                {
                    foreach (Cheat c in cheatSheet)
                    {
                        if (currChar == c.code[currCharIndex].ToString())
                        {
                            expectedCheatId = c.id;
                            NextChar();
                            break;
                        }
                        else ResetInput();
                    }
                }
                else if (currCharIndex < 7)
                {
                    if (currChar == cheatSheet[expectedCheatId].code[currCharIndex].ToString())
                    {
                        NextChar();
                        if (currCheat == cheatSheet[expectedCheatId].code)
                        {
                            ManageCheats(expectedCheatId);
                            ResetInput();
                        }
                    }
                    else ResetInput();
                }
                else
                {
                    ResetInput();
                }
                //Debug.Log("Input " + currChar + ". Current Cheat: " + currCheat + "; Expected Cheat: " + cheatSheet[expectedCheatId].code);
            }
        }
    }

    void NextChar()
    {
        currCheat += currChar;
        currCharIndex++;
    }
    void ResetInput()
    {
        currCheat = "";
        currCharIndex = 0;
        expectedCheatId = 0;
    }

    public void ManageCheats(int id)
    {
        Cheat c = Array.Find(cheatSheet, c => c.id == id);
        if (c.isOn) Disable(c);
        else Enable(c);
    }
    void Enable(Cheat c)
    {
        c.isOn = true;
        StartCoroutine(UpdateText(("Cheat " + c.code + " - " + c.name + " activated.")));
        switch (c.code)
        {
            default:
                c.isOn = false;
                break;
            case "carnack":
                OnHealth();
                OnStamina();
                OnAmmo();
                break;
            case "imusain":
                OnStamina();
                break;
            case "maxammo":
                OnAmmo();
                break;
            case "hesoyam":
                OnHesoyam();
                break;
        }
    }
    void Disable(Cheat c)
    {
        c.isOn = false;
        StartCoroutine(UpdateText(("Cheat " + c.code + " - " + c.name + " deactivated.")));
        switch (c.code)
        {
            case "carnack":
                OffHealth();
                OffStamina();
                OffAmmo();
                break;
            case "imusain":
                OffStamina();
                break;
            case "maxammo":
                OffAmmo();
                break;
            case "hesoyam":
                OffHesoyam();
                break;
        }
    }

    public void OnHealth()
    {
        currHealthPH = playerController.currHealth;
        maxHealthPH = playerController.maxHealth;
        playerController.currHealth = float.MaxValue;
        playerController.maxHealth = float.MaxValue;
    }
    public void OffHealth()
    {
        playerController.currHealth = currHealthPH;
        playerController.maxHealth = maxHealthPH;
        currHealthPH = 0f;
        maxHealthPH = 0f;
    }
    public void OnStamina()
    {
        exhaustRatePH = playerController.exhaustRate;
        jumpExhaustionPH = playerController.jumpExhaustion;
        playerController.exhaustRate = 0f;
        playerController.jumpExhaustion = 0f;
    }
    public void OffStamina()
    {
        playerController.exhaustRate = exhaustRatePH;
        playerController.jumpExhaustion = jumpExhaustionPH;
        exhaustRatePH = 0f;
        jumpExhaustionPH = 0f;
    }
    public void OnAmmo()
    {
        playerController.noAmmoDepletion = true;
    }
    public void OffAmmo()
    {
        playerController.noAmmoDepletion = false;
    }
    public void OnHesoyam()
    {
        playerController.isTargetable = false;
    }
    public void OffHesoyam()
    {
        playerController.isTargetable = true;
    }

    IEnumerator UpdateText(string text)
    {
        AudioManager.instance.PlaySound("Cheat");
        CheatBox.GetComponent<TMP_Text>().alpha = 1f;
        CheatBox.GetComponent<TMP_Text>().text = text;
        while (CheatBox.GetComponent<TMP_Text>().alpha > 0)
        {
            CheatBox.GetComponent<TMP_Text>().alpha-= Time.deltaTime / 3f;
            yield return 0;
        }
    }
}
