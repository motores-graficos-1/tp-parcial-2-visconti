using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Cheat
{
    public string name;
    public string code;
    public int id;
    public bool isOn;
}
