using System;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public MySound[] sounds;
    public static AudioManager instance;
    public PlayerController playerController;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        foreach (MySound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.soundClip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.repeat;
        }
    }


    public void PlaySound(string soundName)
    {
        MySound s = Array.Find(sounds, sound => sound.Name == soundName);
        s.source.volume = playerController.currVolume;
        if (s == null)
        {
            Debug.LogWarning("Sound: " + soundName + " not found.");
            return;
        }
        else
        {
            s.source.Play();
        }
    }
    public void MenuSound(float currVolume)
    {
        string soundName = "Cheat";
        MySound s = Array.Find(sounds, sound => sound.Name == soundName);
        s.source.volume = currVolume;
        if (s == null)
        {
            return;
        }
        else
        {
            s.source.Play();
        }
    }
    public void PauseSound(string soundName)
    {
        MySound s = Array.Find(sounds, sound => sound.Name == soundName);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + soundName + " not found.");
            return;
        }
        else
        {
            s.source.Pause();
        }
    }
}
