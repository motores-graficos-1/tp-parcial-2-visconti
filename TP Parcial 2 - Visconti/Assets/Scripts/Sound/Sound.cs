using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MySound
{
    public string Name;
    public AudioClip soundClip;
    [Range(0f, 1f)] public float volume;
    [Range(0f, 1f)] public float pitch;
    public bool repeat;

    [HideInInspector]
    public AudioSource source;
}
