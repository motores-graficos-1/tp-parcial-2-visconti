using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Weapon
{
    public string name;
    public int nextSlot;
    public int prevSlot;
    public GameObject gameObject;
    public float range;
    public float damage;
    public float useCooldown;
    public bool useAmmo;
    public float ammo;
    public float currentAmmo;
    public float ammoCapacity;
    public float reloadCooldown;
    public float ammoPickupQuantity;
    public GameObject physicalAmmo;
    public float bulletSpeed;
    public GameObject bulletSpawnPoint;
    public float bulletDestroyTime;
    public bool available;
    public bool isActive;
    public Sprite uiIcon;

    public void Constructor (string Name, int NextSlot, int PrevSlot, GameObject GameObject, float baseRange, float baseDamage,
        float baseUseCooldown, bool UseAmmo, float Ammo, float CurrentAmmo, float AmmoCapacity, float ReloadCooldown, float AmmoPickupQuantity, GameObject PhysicalAmmo,
        float BulletSpeed, GameObject BulletSpawnPoint, float BulletDestroyTime, bool Available, bool Active, Sprite UIIcon)
    {
        name = Name;
        nextSlot = NextSlot;
        prevSlot = PrevSlot;
        gameObject = GameObject;
        range = baseRange;
        damage = baseDamage;
        useCooldown = baseUseCooldown;
        useAmmo = UseAmmo;
        ammo = Ammo;
        currentAmmo = CurrentAmmo;
        ammoCapacity = AmmoCapacity;
        reloadCooldown = ReloadCooldown;
        ammoPickupQuantity = AmmoPickupQuantity;
        physicalAmmo = PhysicalAmmo;
        if (useAmmo == false) { physicalAmmo = null; }
        bulletSpeed = BulletSpeed;
        bulletSpawnPoint = BulletSpawnPoint;
        bulletDestroyTime = BulletDestroyTime;
        available = Available;
        isActive = Active;
        uiIcon = UIIcon;
    }
}
