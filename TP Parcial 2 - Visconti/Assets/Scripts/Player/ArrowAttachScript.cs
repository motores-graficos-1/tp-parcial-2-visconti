using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowAttachScript : MonoBehaviour
{
    [SerializeField] private GameObject bloodParticles;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag != "Player")
        {
            if (this.transform.parent == null)
            {
                this.GetComponent<Rigidbody>().velocity = Vector3.zero;
                this.GetComponent<Rigidbody>().isKinematic = true;
                this.transform.SetParent(other.gameObject.transform);
                if (other.gameObject.tag == "Enemy")
                {
                    Instantiate(bloodParticles, transform.position, Quaternion.identity);
                }
            }

        }
    }
}
