using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class KeyItem
{
    public string name;
    public bool available;
    public int keyCode;
    public string hint;
    public Sprite uiIcon;
    public Sprite disabledIcon;
    public GameObject iconObject;
    public RectTransform UIPosition;

    public void Constructor(string Name, bool Available, int KeyCode)
    {
        name = Name;
        available = Available;
        keyCode = KeyCode;
    }
}
