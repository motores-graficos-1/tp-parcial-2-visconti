using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    Vector2 mouseLook;
    Vector2 vSmoothness;
    [SerializeField] private float sensibility = 5.0f;
    [SerializeField] private float smoothness = 2.0f;
    GameObject player;

    void Start()
    {
        player = transform.parent.gameObject;
    }
    void Update()
    {
        if (player.GetComponent<PlayerController>().inGame == false)
        {
            return;
        }
        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

        md = Vector2.Scale(md, new Vector2(sensibility * smoothness, sensibility * smoothness));

        vSmoothness.x = Mathf.Lerp(vSmoothness.x, md.x, 1f / smoothness);
        vSmoothness.y = Mathf.Lerp(vSmoothness.y, md.y, 1f / smoothness);

        mouseLook += vSmoothness;
        mouseLook.y = Mathf.Clamp(mouseLook.y, -90f, 90f); //Limit rotation in Y axis to -90/90 degrees
        transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
        player.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, player.transform.up);

    }
}