using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerController : MonoBehaviour
{
    [Header("Technical Properties")]
    //Self Components
    private Rigidbody rb;
    [SerializeField] private Camera playerCamera;
    [SerializeField] private LayerMask enemyLayer;
    //Gounded Variables
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] private float height;
    [SerializeField] private float maxSlopeAngle;
    private RaycastHit slopeHit;
    //Random
    private float bobbingModifier = 0f;
    private float footstepTimer = 0f;
    [SerializeField] private Animator weaponAnimator;
    public float flashingColor;
    public bool inGame = true;
    private bool gameOver = false;
    public float timeScalePH;
    public float currVolume;
    [Header("Stats")]
    //Health and Stamina
    [SerializeField] public float currHealth = 100f;
    [SerializeField] public float maxHealth = 100f;
    [SerializeField] public float currStamina = 100f;
    [SerializeField] public float maxStamina = 100f;
    [SerializeField] public float exhaustRate = 10f;
    [SerializeField] public float jumpExhaustion = 20f;
    [Header("UI")]
    [SerializeField] private BarScript healthBar;
    [SerializeField] private BarScript staminaBar;
    [SerializeField] TMP_Text healthLegend;
    [SerializeField] TMP_Text staminaLegend;
    [SerializeField] TMP_Text hintsLegend;
    [SerializeField] private GameObject weaponIcon;
    [SerializeField] private TMP_Text ammoLegend;
    [SerializeField] private TMP_Text helpLegend;
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private GameObject optionsMenu;
    [SerializeField] private GameObject gameOverMenu;
    [SerializeField] private GameObject winMenu;
    [Header("Movement")]
    //Move Variables
    [SerializeField] private float walkSpeed;
    [SerializeField] private float runSpeed;
    [SerializeField] private float slowSpeed;
    private bool isExhausted = false;
    private bool isRunning = false;
    private float hMov;
    private float vMov;
    private Vector3 movDirection;
    //Jump Variables
    [SerializeField] private float groundDrag;
    [SerializeField] private float jumpForce;
    [Header("Combat")]
    [SerializeField] public Weapon[] weapons;
    [SerializeField] private Weapon currWeapon;
    private float currCooldown = 0f;
    private bool isReloading = false;
    [Header("Inventory")]
    [SerializeField] private float interactionRange = 4f;
    [SerializeField] public KeyItem[] keyStorage;
    [Header("Cheats")]
    public bool noAmmoDepletion = false;
    public bool isTargetable = true;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
        DeclareWeapons();
        //Set Health
        currHealth = maxHealth;
        healthBar.SetMaxFill(maxHealth);
        //Set Stamina
        currStamina = maxStamina;
        staminaBar.SetMaxFill(maxStamina);
    }
    void Update() //Inputs
    {
        if (gameOver) { inGame = false; }
        CheckInput();
        currVolume = optionsMenu.GetComponentInChildren<Slider>().value;
        flashingColor = Mathf.Sin(Time.time*4)/2+0.5f;
        if (currHealth <= 0f) { GameOver(gameOverMenu); }
    }
    void FixedUpdate() //Movements and Physics
    {
        HandleStamina();
        MovePlayer();
        UpdateUI();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "WeaponPickup")
        {
            GrabWeapon(other);
            Destroy(other.gameObject);
        }
        if (other.gameObject.tag == "Consumable")
        {
            GrabItem(other);
            Destroy(other.gameObject);
        }
        if (other.transform.parent.name == "FIN")
        {
            Destroy(other);
            GameOver(winMenu);
        }
    }

    //Input & Movement
    void CheckInput()
    {
        if (inGame == false)
        {
            return;
        }
        //Pause Game
        if (Input.GetKeyDown(KeyCode.Escape)) { PauseGame(pauseMenu); }
        //Get Moving Speed State
        if ((Input.GetAxis("WalkState") > 0f) && isExhausted == false) { isRunning = true; }
        else { isRunning = false; }
        //Get Moving Direction Input
        hMov = Input.GetAxis("Horizontal");
        vMov = Input.GetAxis("Vertical");
        //Camera Animation
        CameraBobbing();
        //Jump
        if (Input.GetKeyDown(KeyCode.Space) && Grounded() && isExhausted == false) { JumpPlayer(); }
        //Attack
        if (Input.GetMouseButtonDown(0)) { Attack(); }
        //Reload Weapon
        if (Input.GetKeyDown(KeyCode.R) && isReloading == false) { StartCoroutine(WeaponReload()); }
        //Unlock cursor
        if (Input.GetKeyDown(KeyCode.Escape)) { Cursor.lockState = CursorLockMode.None; }
        //Weapon Selection
        if (Input.GetAxis("Mouse ScrollWheel") < 0) { SwapWeapon(currWeapon.nextSlot); }
        else if (Input.GetAxis("Mouse ScrollWheel") > 0) { SwapWeapon(currWeapon.prevSlot); }
        if (Input.GetKeyDown(KeyCode.Alpha1)) { SwapWeapon(0); }
        if (Input.GetKeyDown(KeyCode.Alpha2)) { SwapWeapon(1); }
        if (Input.GetKeyDown(KeyCode.Alpha3)) { SwapWeapon(2); }
        //Interact Raycast
        if (Input.GetKeyDown(KeyCode.E)) { Interact(); }
    }
    void MovePlayer()
    {
        movDirection = transform.forward * vMov + transform.right * hMov;           //Get Movement Direction
        float finalMovSpeed = (isRunning) ? runSpeed : walkSpeed;                   //Calculate different speed walking/running
        if (Grounded()) //Apply move force and drag to player relative to the floor angle
        {
            rb.AddForce(10f * finalMovSpeed * GetSlopeMoveDirection(), ForceMode.Force);
            rb.drag = groundDrag;
            if (isRunning == true) { currStamina -= Time.deltaTime * exhaustRate; } //Decrease Stamina when running
        }
        else { rb.drag = 0.1f; }
    }
    void JumpPlayer()
    {
        currStamina -= jumpExhaustion;
        //Zero out the vertical speed
        rb.velocity = new Vector3(rb.velocity.x, 0f, rb.velocity.z);
        //Apply jump force to player
        rb.AddForce(transform.up * jumpForce, ForceMode.Impulse);
    }
    bool IsMoving()
    {
        return ((Math.Abs(hMov) + Math.Abs(vMov)) > 0.1f);
    }
    bool Grounded()
    {
        return Physics.Raycast(transform.position, Vector3.down, out slopeHit, height * 0.5f + 0.3f, groundLayer);
    }
    void Footsteps()
    {
        //Sound
        //Randomize between 2 step sounds
        int i = UnityEngine.Random.Range(0, 2);
        switch (slopeHit.collider.gameObject.tag)
        {
            case "Dirt":
                string[] dirtStep = { "Dirt0", "Dirt1" };
                AudioManager.instance.PlaySound(dirtStep[i]);
                footstepTimer = 3f;
                break;
            case "Stone":
                string[] stoneStep = { "Stone0", "Stone1" };
                AudioManager.instance.PlaySound(stoneStep[i]);
                footstepTimer = 3f;
                break;
        }
    }
    void CameraBobbing()
    {
        if (IsMoving() && Grounded())
        {
            //Calculate
            if (rb.velocity.magnitude > 0.1f) { bobbingModifier += 0.01f; }
            if (bobbingModifier >= 2 * Math.PI) { bobbingModifier = 0f; }
            footstepTimer -= rb.velocity.magnitude*Time.deltaTime;
            //Camera
            playerCamera.transform.position = transform.position + new Vector3(0f, 0.85f + (Mathf.Sin(bobbingModifier) / 3), 0f); //Oscillate in the Y position
            //Weapon Animation
            weaponAnimator.SetBool("IsMoving", true);
            if (footstepTimer <= 0) { Footsteps(); }
        }
        else { weaponAnimator.SetBool("IsMoving", false); }
    }
    Vector3 GetSlopeMoveDirection()
    {
        return Vector3.ProjectOnPlane(movDirection, slopeHit.normal).normalized; //Return the normalized vector of the slope below
    }
    void HandleStamina()
    {
        //Stop Regenerating if running
        if (isRunning) { StopCoroutine(StaminaRegen()); }
        //Activate Exhaustion when current stamina is less than exhaustion rate
        if (currStamina <= 0f) { isExhausted = true; }
        //If the player is not running, re-enable player running when 0.75% of the stamina is recovered
        if (isRunning == false)
        {
            StartCoroutine(StaminaRegen());
            if (currStamina >= maxStamina * 0.75f) { isExhausted = false; }
        }
    }
    IEnumerator StaminaRegen()
    {
        while (isRunning == false && currStamina < maxStamina)
        {
            currStamina += Time.deltaTime / (exhaustRate / 2);
            yield return null;
        }
    }

    //Combat & Interaction
    void DeclareWeapons()
    {
        currWeapon = weapons[0];
        currWeapon.gameObject.GetComponent<MeshRenderer>().enabled = true;
        weaponAnimator = currWeapon.gameObject.GetComponent<Animator>();
    }
    void GrabWeapon(Collider item)
    {
        for (int i = 0; i < weapons.Length; i++)
        {
            if (item.name == weapons[i].name)
            {
                weapons[i].available = true;
                SwapWeapon(i);
            }
        }
    }
    void GrabItem(Collider item)
    {
        if (item.name.Contains("MedKit"))
        {
            StartCoroutine(TakeHealing(25f));
        }
        else if (item.name.Contains("Key"))
        {
            for (int i = 0; i < keyStorage.Length; i++)
            {
                if (item.name == keyStorage[i].name)
                {
                    keyStorage[i].available = true;
                    break;
                }
            }
        }
        else
        {
            string weapon = item.name.Substring(0, 3);
            for (int i = 0; i < weapons.Length; i++)
            {
                if (weapons[i].name.Substring(0, 3) == weapon)
                {
                    weapons[i].ammo += weapons[i].ammoPickupQuantity;
                    break;
                }
            }
        }
    }
    void SwapWeapon(int weaponKey)
    {
        if (weaponKey < weapons.Length)
        {
            if (weapons[weaponKey].available == true)
            {
                //Prevent reloading to carry over to another weapon
                isReloading = false;
                //Copy current stats to the weapon in the array
                for (int i = 0; i < weapons.Length; i++)
                {
                    if (currWeapon.name == weapons[i].name)
                    {
                        currWeapon.isActive = false; //Deactivate Weapon
                        weapons[i].ammo = currWeapon.ammo; //Set Total Ammo
                        weapons[i].currentAmmo = currWeapon.currentAmmo; //Set Current Ammo
                        weapons[i].isActive = currWeapon.isActive; //Set Activation
                        weapons[i].gameObject.GetComponent<MeshRenderer>().enabled = false;
                    }
                }
                //Activate selected weapon
                weapons[weaponKey].isActive = true;
                weapons[weaponKey].gameObject.GetComponent<MeshRenderer>().enabled = true;
                //Set selected weapon as current weapon from the array
                currWeapon = weapons[weaponKey];
                //Set selected weapon's Animator
                weaponAnimator = currWeapon.gameObject.GetComponent<Animator>();
            }
            else { Debug.Log("Weapon "+weaponKey+" is not available!"); }
        }
    }
    void Attack()
    {
        //Re-Lock cursor
        if (Cursor.lockState == CursorLockMode.None) { Cursor.lockState = CursorLockMode.Locked; }
        if (currCooldown <= 0) //Check current cooldown before shooting
        {
            //If the player is not reloading
            if (isReloading == false)
            {
                //If the current weapon has ammo loaded
                if (currWeapon.currentAmmo > 0)
                {
                    StartCoroutine(WeaponCooldown());
                    //Perform the shot depending on the weapon array
                    switch (currWeapon.name)
                    {
                        case "Stake":
                            //Random Sound between 3 choices
                            string[] sound = { "Attack0", "Attack1", "Attack2" };
                            int i = UnityEngine.Random.Range(0, 3);
                            AudioManager.instance.PlaySound(sound[i]);
                            weaponAnimator.SetTrigger("Attack");
                            ShootWeapon();
                            break;
                        case "Revolver":
                            AudioManager.instance.PlaySound("Revolver0");
                            weaponAnimator.SetTrigger("Attack");
                            ShootWeapon();
                            break;
                        case "Crossbow":
                            AudioManager.instance.PlaySound("Crossbow0");
                            weaponAnimator.SetTrigger("Attack");
                            ShootWeapon();
                            break;
                    }
                }

                //If it doesn't have ammo loaded but has ammo in stock
                //else if (currWeapon.ammo > 0) { StartCoroutine(WeaponReload()); } //REMOVED (CAN BE RE-ADDED)

                //If it doesn't have ammo loaded or in stock
                else { AudioManager.instance.PlaySound("No Ammo"); }
            }
        }
    }
    void ShootWeapon()
    {
        RaycastHit hit = ShootRay(currWeapon.range, currWeapon.damage);
        //If there's ammo reloaded
        if (currWeapon.useAmmo == true)
        {
            //Instantiate The bullet with the respective gun parent
            GameObject shot = Instantiate<GameObject>(currWeapon.physicalAmmo, currWeapon.bulletSpawnPoint.transform.position, currWeapon.bulletSpawnPoint.transform.rotation, currWeapon.gameObject.transform);
            //Remove the parent keeping the world-relative transform
            shot.transform.SetParent(null, true);
            //Set rotation to be straight from the gun, or to point to the place where the raycast hit
            if (hit.collider == null)
            {
                shot.transform.up = currWeapon.gameObject.transform.forward;
            }
            else
            {
                Vector3 shotDirection = new(hit.point.x - shot.transform.position.x, hit.point.y - shot.transform.position.y, hit.point.z - shot.transform.position.z);
                shot.transform.up = shotDirection;
            }
            //Set Impulse to ammo
            shot.GetComponent<Rigidbody>().AddForce(shot.transform.up * currWeapon.bulletSpeed, ForceMode.Impulse);
            Destroy(shot, currWeapon.bulletDestroyTime);
            //Decrease current ammo
            if (noAmmoDepletion == false)
            {
                currWeapon.currentAmmo--;
            }
        }
    }
    IEnumerator WeaponCooldown()
    {
        currCooldown = currWeapon.useCooldown;
        while (currCooldown >= 0)
        {
            currCooldown -= Time.deltaTime;
            yield return null;
        }
    }
    IEnumerator WeaponReload()
    {
        //Stop all other shooting/reloading actions from happening while reloading
        isReloading = true;
        //Check how much ammo should the coroutine load, based on inventory ammo, current loaded ammo and max barrel capacity
        //If there's more ammo left than the capacity-current ammo, load the remaining ammo to reach the capacity; If not, load all the remaining ammo
        float nextReload = (currWeapon.ammo >= currWeapon.ammoCapacity - currWeapon.currentAmmo) ? currWeapon.ammoCapacity - currWeapon.currentAmmo : currWeapon.ammo;
        for (int i=0; i < nextReload; i++)
        {
            //Checking if the player is still reloading to prevent carrying the reload to a different weapon
            if (isReloading)
            {
                currWeapon.currentAmmo++;
                currWeapon.ammo--;
                AudioManager.instance.PlaySound("Ammo Reload");
            }
            yield return new WaitForSeconds(currWeapon.reloadCooldown);
        }
        isReloading = false;
    }
    public void InvokeTakeDamage(float amount)
    {
        StartCoroutine(TakeDamage(amount));
    }
    IEnumerator TakeDamage(float amount)
    {
        float previousHealth = currHealth;
        float waitFor = 0.25f/amount/2f;
        while (currHealth > previousHealth - amount)
        {
            currHealth -= 0.1f;

            yield return new WaitForSeconds(waitFor);
        }
    }
    IEnumerator TakeHealing(float amount)
    {
        float previousHealth = currHealth;
        while (currHealth < previousHealth + amount)
        {
            currHealth += Time.deltaTime * 25f;
            if (currHealth >= maxHealth)
            {
                currHealth = maxHealth;
                break;
            }
            yield return null;
        }
    }
    void Interact()
    {
        InteractRay(interactionRange);
    }

    //UI
    //Ingame UI
    void UpdateUI()
    {
        //HealthBar Update
        healthBar.SetFill(currHealth);

        //StaminaBar Update
        staminaBar.SetFill(currStamina);
        //Alternate text between Exhausted and Stamina
        if (isExhausted == false) { staminaLegend.text = "Stamina"; staminaLegend.color = Color.white; }
        else { staminaLegend.color = new Color(1, flashingColor, flashingColor); staminaLegend.text = "Exhausted!"; }

        //Ammo displayed or not depending on the weapon
        ammoLegend.text = (currWeapon.useAmmo) ? " " + currWeapon.currentAmmo + " / " + currWeapon.ammo : ammoLegend.text = "";
        //Ammo Text Style change when chamber is empty
        if (currWeapon.currentAmmo <= 0) { ammoLegend.color = new Color(1, flashingColor, flashingColor); }
        else { ammoLegend.color = Color.white; }
        //Weapon Icon Update
        weaponIcon.GetComponent<Image>().sprite = currWeapon.uiIcon;
        weaponIcon.transform.Find("IconBG").GetComponent<Image>().sprite = currWeapon.uiIcon;
        //Weapon Icon ammo depletion
        weaponIcon.GetComponent<Image>().fillAmount = (currWeapon.currentAmmo / currWeapon.ammoCapacity);

        //Key Icons Update
        for (int i = 0; i < keyStorage.Length; i++)
        {
            if (keyStorage[i].available != true) { keyStorage[i].iconObject.GetComponent<Image>().color = new Color(1f, 1f, 1f, 0f); }
            else { keyStorage[i].iconObject.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f); }
        }
    }
    
    void PauseGame(GameObject menuObject)
    {
        if (inGame == true)
        {
            inGame = false;
            Cursor.lockState = CursorLockMode.None; Cursor.visible = true;
            timeScalePH = Time.timeScale;
            OpenMenu(menuObject);
            Time.timeScale = 0f;
        }
    }
    void OpenMenu(GameObject menuObject)
    {
        helpLegend.gameObject.SetActive(true);
        menuObject.gameObject.SetActive(true);
    }
    public void CloseMenu()
    {
        helpLegend.gameObject.SetActive(false);
        pauseMenu.gameObject.SetActive(false);
    }

    void GameOver(GameObject menuObject)
    {
        PauseGame(menuObject);
        gameOver = true;
    }
    RaycastHit ShootRay(float range, float damage)
    {
        Ray ray = playerCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f)); //Create a Raycast from the center of the screen
        RaycastHit hit; //Create a RaycastHit variable for the raycast

        //If you hit something and it is within range
        if ((Physics.Raycast(ray, out hit) == true) && hit.distance < range)
        {
            //Enemy Collision
            if (hit.collider.name.Substring(0, 5) == "Enemy")
            {
                //Get which of the enemies was collided
                GameObject hitObject = hit.transform.gameObject;
                //Get the collided enemy's AI script
                EnemyController hitObjectScript = (EnemyController)hitObject.GetComponent(typeof(EnemyController));
                //If the AI script exists (to avoid crashing if the script component was not added)
                if (hitObjectScript != null)
                {
                    hitObjectScript.TakeDamage(currWeapon.damage); //Execute the take damage method
                }
            }
        }
        return hit;
    }
    RaycastHit InteractRay(float range)
    {
        Ray ray = playerCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f)); //Create a Raycast from the center of the screen
        RaycastHit hit; //Create a RaycastHit variable for the raycast

        if ((Physics.Raycast(ray, out hit) == true) && hit.distance < range)
        {
            //Door Interaction
            if (hit.collider.name.Contains("Door"))
            {
                GameObject doorObject = hit.transform.gameObject;
                Door doorObjectScript = (Door)doorObject.transform.parent.GetComponent(typeof(Door));
                if (doorObjectScript.keyRequired == true)
                {
                    for (int i = 0; i < keyStorage.Length; i++)
                    {
                        if (keyStorage[i].keyCode == doorObjectScript.keyCode)
                        {
                            if (keyStorage[i].available != true)
                            {
                                StartCoroutine(HintText(keyStorage[i].hint));
                                Debug.Log("You are missing the " + keyStorage[i].name);
                            }
                            else
                            {
                                doorObjectScript.Toggle();
                                keyStorage[i].iconObject.GetComponent<Image>().sprite = keyStorage[0].disabledIcon;
                            }
                        }
                    }
                }
                else
                {
                    doorObjectScript.Toggle();
                }
            }
        }
        return hit;
    }
    IEnumerator HintText(string text)
    {
        hintsLegend.alpha = 1f;
        hintsLegend.text = text;
        while (hintsLegend.alpha > 0)
        {
            hintsLegend.alpha -= Time.deltaTime / 3f;
            yield return 0;
        }
    }
}