using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] string playScene;
    [SerializeField] GameObject optionsMenu;
    //[SerializeField] GameObject optionsMenu;

    public void Play()
    {
        StartCoroutine(LoadGame());        
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void PlaySound()
    {
        AudioManager.instance.MenuSound(optionsMenu.GetComponentInChildren<Slider>().value);
    }

    IEnumerator LoadGame()
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(playScene, LoadSceneMode.Single);
    }
}
