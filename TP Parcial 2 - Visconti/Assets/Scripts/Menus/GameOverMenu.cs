using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverMenu : MonoBehaviour
{
    [SerializeField] PlayerController player;
    [SerializeField] GameObject optionsMenu;
    [SerializeField] string playScene;
    [SerializeField] string menuScene;

    public void Restart()
    {
        Time.timeScale = 1f;
        Cursor.lockState = CursorLockMode.Locked; Cursor.visible = false;
        StartCoroutine(LoadGame());
    }

    public void MainMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(menuScene);
    }

    public void PlaySound()
    {
        AudioManager.instance.MenuSound(optionsMenu.GetComponentInChildren<Slider>().value);
    }

    IEnumerator LoadGame()
    {
        yield return new WaitForSeconds(1);
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
}
