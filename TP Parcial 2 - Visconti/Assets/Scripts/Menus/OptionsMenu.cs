using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Audio;

public class OptionsMenu : MonoBehaviour
{
    [SerializeField] private AudioMixer mixer;
    [SerializeField] TMP_Text volumeText;
    float volumePH = 1f;

    void Update()
    {
        volumeText.text = (System.Math.Round(volumePH * 100f,0)).ToString();
    }

    public void SetVolume(float value)
    {
        volumePH = value;
        if (value > 0)
        {
            mixer.SetFloat("MasterVolume", Mathf.Log10(value) * 20);
        }
        else mixer.SetFloat("MasterVolume", -80f);
    }
    public void PlaySound()
    {
        AudioManager.instance.MenuSound(volumePH);
    }
}
