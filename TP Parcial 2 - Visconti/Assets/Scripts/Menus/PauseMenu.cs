using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] PlayerController player;
    [SerializeField] GameObject optionsMenu;
    [SerializeField] string menuScene;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Resume();
        }
    }
    public void Resume()
    {
        Time.timeScale = player.timeScalePH;
        Cursor.lockState = CursorLockMode.Locked; Cursor.visible = false;
        player.inGame = true;
        player.CloseMenu();
    }

    public void MainMenu()
    {
        Time.timeScale = player.timeScalePH;
        SceneManager.LoadScene(menuScene);
    }

    public void PlaySound()
    {
        AudioManager.instance.MenuSound(optionsMenu.GetComponentInChildren<Slider>().value);
    }
}
